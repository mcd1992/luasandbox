local luadir = "../lua/src"

solution "luasandbox"
    configurations { "Debug", "Release" }

    project "luasandbox"
        kind "ConsoleApp"
        language "C++"
        location( os.get() .."-".. _ACTION )
        targetdir( "bin/" .. os.get() .. "/" )

        includedirs { luadir }

        libdirs { luadir }
        links {
            "dl",       -- libdl, required for luaL_openlibs
            ":liblua.a" -- liblua.a file compiled & located in luadir
        }

        files { "**.h", "**.cpp" }

        configuration "Debug"
            defines { "DEBUG" }
            flags { "Symbols" }

        configuration "Release"
            defines { "NDEBUG" }
            flags { "Optimize" }
