#include <stdio.h>
#include <new>
#include "lua.hpp"

class MyObj {
	public:
		int val;
		MyObj( int );
};

MyObj::MyObj( int a ) { // Constructor
	val = a;
};

int MyObj_New( lua_State *L ) {
	int initval = luaL_checkinteger( L, 1 ); // Assign the first argument to initval

	MyObj *ud = (MyObj*)lua_newuserdata( L, sizeof(MyObj) );
	// lua_newuserdata allocates sizeof(uint) memory for our userdata

	new (ud) MyObj( initval );
	// does not allocate memory, calls new (sizeof(MyObj), ud)
	// this constructs the object at the address pointed at by ud

	luaL_setmetatable( L, "MyObj" ); // Set our userdata's metatable to _R.MyObj

	return 1; // Return the userdata
}

int MyObj_GC( lua_State *L ) {
	void *ud = luaL_checkudata( L, 1, "MyObj" );

	MyObj* obj = (MyObj*)ud;
	printf( "garbage collected a MyObj object: 0x%x [val = %i]\n", obj, obj->val );
}

int MyObj_Get( lua_State *L ) {
	void *ud = luaL_checkudata( L, 1, "MyObj" ); // Make sure the first argument is `MyObj`

	MyObj* obj = (MyObj*)ud;
	// ud is a pointer to the address of our MyObj object

	lua_pushinteger( L, obj->val );

	return 1;
}

int MyObj_Set( lua_State *L ) {
	void *ud = luaL_checkudata( L, 1, "MyObj" );
	luaL_argcheck( L, ud != NULL, 1, "invalid userdata!");

	int setval = luaL_checkinteger( L, 2 );

	MyObj* obj = (MyObj*)ud;
	obj->val = setval;

	lua_pushnil( L );
	return 1; // return nil
}

static const struct luaL_Reg myobj_metatable[] = {
	{"__gc", MyObj_GC},
	{"Get",  MyObj_Get},
	{"Set",  MyObj_Set},
	{NULL, NULL}
};

static const struct luaL_Reg myobj_libfunctions[] = {
	{"new", MyObj_New},
	{NULL, NULL}
};

LUAMOD_API int luaopen_myobj( lua_State *L ) {
	lua_pop( L, 1 ); // Remove "myobj" string from stack; pushed by luaL_requiref

	luaL_newmetatable( L, "MyObj" );        // Create & push the metatable for our MyObj
	luaL_setfuncs( L, myobj_metatable, 0 ); // Add the metamethods defined in myobj_metatable to the metatable

	luaL_getmetatable( L, "MyObj" );  // Push another copy of _R.MyObj onto the stack
	lua_setfield( L, -2, "__index" ); // Set _R.MyObj.__index to _R.MyObj

	lua_pop( L, 1 ); // Remove _R.MyObj table from stack
	luaL_newlib( L, myobj_libfunctions ); // Creates a table with the given struct's kv pairs
	// This is to be used as the _G.myobj table
	return 1; // return the _G.myobj table
}
