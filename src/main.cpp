#include <new>
#include <stdio.h>
#include "lua.hpp"

#define nullptr (void*)NULL

LUAMOD_API int luaopen_myobj( lua_State *L ); // Prototype to setup MyObj metatable

int main( int argc, char **arg ) {
	lua_State *L = luaL_newstate(); // Create Lua state
	if ( L == nullptr ) {
		printf( "Failed to create lua state! OOM!" );
		return -1;
	}

	luaL_openlibs( L ); // Open standard libraries
	luaL_requiref( L, "myobj", &luaopen_myobj, 1 ); // Setup our MyObj userdata
	lua_pop( L, 1 ); // luaL_requiref pushes the myobj table onto the stack

	// Load our init.lua
	int error = luaL_dofile( L, "init.lua" );
	if ( error ) {
		printf( "\nLua ERROR: %s\n\n", luaL_checkstring( L, -1 ) );
		lua_pop( L, 1 ); // remove error
	}

	int stacksize = lua_gettop( L );
	if ( stacksize != 0 ) {
		printf( "Warning: %i object[s] left on the Lua stack! Removing...\n", stacksize );
		lua_pop( L, stacksize );
	}

	lua_close(L);
	return 0;
}
